import UIKit

class ListCitiesViewController: UIViewController {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    private var arrayOfCities: [WeatherClass] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        self.loadArray()
        self.tableView.rowHeight = self.view.frame.height/12
    }
    
    private func loadArray () {
        
        if let load = Manager.shared.loadArrayOfCities() {
            self.arrayOfCities = load
        }
    }
}

extension ListCitiesViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayOfCities.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CityTableViewCell", for: indexPath) as? CityTableViewCell else {
            return UITableViewCell()
        }
        
        if let name = arrayOfCities[indexPath.row].location?.name,
            let temp_c = arrayOfCities[indexPath.row].current?.temp_c,
            var localtime = arrayOfCities[indexPath.row].location?.localtime,
            let text = arrayOfCities[indexPath.row].current?.condition?.text,
            let is_day = arrayOfCities[indexPath.row].current?.is_day {
            
            localtime.removeFirst(11)
            
            cell.cityLabel.text = name
            cell.temperatureLabel.text = "\(temp_c)°"
            cell.timeLabel.text = "\(localtime)"
            cell.backgroundImageView.image = Manager.shared.addBackgroundImage(text: text, is_day: is_day)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let DeleteCityAction = UIContextualAction(style: .normal, title:  "Delete", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            
            let numberOfRow = indexPath.row
            if numberOfRow == 0 {
                self.errorDeletingCurrentCityWeatherAlert()
            } else {
                Manager.shared.saveArrayOfCitiesAfterDeleting(numberOfRow: numberOfRow)
                self.loadArray()
                self.tableView.reloadData()
            }
            success(true)
        })
        DeleteCityAction.backgroundColor = .red
        return UISwipeActionsConfiguration(actions: [DeleteCityAction])
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let numberOfRow = indexPath.row
        Manager.shared.jsonWeather = Manager.shared.loadArrayOfCities()?[numberOfRow]
        self.navigationController?.popToRootViewController(animated: true)
    }
}
