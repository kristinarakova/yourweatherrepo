import Foundation
import UIKit

extension UIViewController {
    
    func errorDeletingCurrentCityWeatherAlert () {
        let alert = UIAlertController(title: "Error", message: "Sorry, you can't delete the current city", preferredStyle: .alert)
        let firstAction = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
        alert.addAction(firstAction)
        self.present(alert, animated: true, completion: nil)
    }
}

extension UIViewController {
    
    func dateFormatter (data: String, oldFormat: String, newFormat: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = oldFormat
        let formatting = formatter.date(from: data)
        formatter.dateFormat = newFormat
        let newDate = formatter.string(from: formatting!)
        return newDate
    }
}

extension UserDefaults {
    
    func set<T: Encodable>(encodable: T, forKey key: String) {
        if let data = try? JSONEncoder().encode(encodable) {
            set(data, forKey: key)
        }
    }
    
    func value<T: Decodable>(_ type: T.Type, forKey key: String) -> T? {
        if let data = object(forKey: key) as? Data,
           let value = try? JSONDecoder().decode(type, from: data) {
            return value
        }
        return nil
    }
}

extension UIViewController: UITextFieldDelegate {
    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func hideKeyboard () {
        let recognizer: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(recognizer)
    }
    
    @objc func dismissKeyboard () {
        view.endEditing(true)
    }
}
