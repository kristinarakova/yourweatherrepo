import Foundation
import UIKit

class Manager {
    
    enum Keys: String {
        case weather = "weather"
        case weatherArray = "arrayWeatherClass"
    }
    
    static let shared = Manager()
    private init () {}
    
    var jsonWeather: WeatherClass?
    var arrayOfCities: [WeatherClass] = []
    
    var index = 0
    
    var is_exist = false
    
    func sendCurrentWeatherRequestByCoordinate(coordinate: String, completion: @escaping ()->()) {
        
        guard let url = URL(string: "https://api.weatherapi.com/v1/forecast.json?key=a60d898ac1c14499922120209202209&q=\(coordinate)&days=3") else {return}
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            if error == nil, let data = data {
                do {
                    let jsonResponce = try JSONDecoder().decode(WeatherClass.self, from: data)
                    print()
                    
                    Manager.shared.save(object: jsonResponce)
                    Manager.shared.jsonWeather = Manager.shared.load()
                    
                    Manager.shared.saveArrayOfCitiesDueCordinate(object: jsonResponce)
                    
                    DispatchQueue.main.async {
                        completion()
                    }
                    
                } catch {
                    print("Data error")
                }
            } else {
                print("error")
            }
        }
        task.resume()
    }
    
    func sendCurrentWeatherRequestByCity(city: String, completion: @escaping ()->()) {
        
        guard let url = URL(string: "https://api.weatherapi.com/v1/forecast.json?key=a60d898ac1c14499922120209202209&q=\(city)&days=3") else {return}
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            if error == nil, let data = data {
                do {
                    let jsonResponce = try JSONDecoder().decode(WeatherClass.self, from: data)
                    print()
                    
                    Manager.shared.save(object: jsonResponce)
                    Manager.shared.jsonWeather = Manager.shared.load()
                    
                    Manager.shared.saveArrayOfCities(object: jsonResponce)
                    
                    DispatchQueue.main.async {
                        completion()
                    }
                    
                } catch {
                    print("Data error")
                }
            } else {
                print("error")
            }
        }
        task.resume()
    }
    
    func sendCityRequest(city: String, completion: @escaping ()->()) {
        
        guard let url = URL(string: "https://api.weatherapi.com/v1/forecast.json?key=a60d898ac1c14499922120209202209&q=\(city)&days=3") else {return}
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            if error == nil, let data = data {
                do {
                    let jsonResponce = try JSONDecoder().decode(WeatherClass.self, from: data)
                    
                    Manager.shared.save(object: jsonResponce)
                    
                    DispatchQueue.main.async {
                        completion()
                    }
                    
                } catch {
                    print("Data error")
                }
            } else {
                print("error")
            }
        }
        task.resume()
    }
    
    func refreshData (city: String, completion: @escaping ()->()) {
        
        guard let url = URL(string: "https://api.weatherapi.com/v1/forecast.json?key=a60d898ac1c14499922120209202209&q=\(city)&days=3") else {return}
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            if error == nil, let data = data {
                do {
                    let jsonResponce = try JSONDecoder().decode(WeatherClass.self, from: data)
                    print()
                    
                    Manager.shared.save(object: jsonResponce)
                    Manager.shared.jsonWeather = Manager.shared.load()
                    
                    Manager.shared.saveArrayAfterRefresh(object: jsonResponce)
                    
                    DispatchQueue.main.async {
                        completion()
                    }
                    
                } catch {
                    print("Data error")
                }
            } else {
                print("error")
            }
        }
        task.resume()
    }
    
    func save(object: WeatherClass) {
        UserDefaults.standard.set(encodable: object, forKey: "weather")
    }
    
    func load() -> WeatherClass? {
        let weather = UserDefaults.standard.value(WeatherClass.self, forKey: "weather")
        return weather
    }
    
    func saveArrayOfCities(object: WeatherClass) {
        
        var arrayOfCities = Manager.shared.loadArrayOfCities()
        
        if let arrayOfCities = arrayOfCities {
            for element in arrayOfCities {
                if element.location?.name == object.location?.name {
                    self.is_exist = true
                }
            }
        }
        
        if self.is_exist == false {
            arrayOfCities?.append(object)
            UserDefaults.standard.set(encodable: arrayOfCities, forKey: "arrayWeatherClass")
        }
        
        self.is_exist = false
    }
    
    func saveArrayAfterRefresh(object: WeatherClass) {
        
        var index = 0
        
        if var array = Manager.shared.loadArrayOfCities() {
            for element in array {
                if element.location?.name != object.location?.name {
                    index += 1
                } else if element.location?.name == object.location?.name {
                    array.remove(at: index)
                    array.insert(object, at: index)
                    UserDefaults.standard.set(encodable: array, forKey: "arrayWeatherClass")
                }
            }
        }
    }
    
    func saveArrayOfCitiesAfterDeleting(numberOfRow: Int) {
        
        var arrayOfCities = Manager.shared.loadArrayOfCities()
        arrayOfCities?.remove(at: numberOfRow)
        UserDefaults.standard.set(encodable: arrayOfCities, forKey: "arrayWeatherClass")
    }
    
    func saveArrayOfCitiesDueCordinate(object: WeatherClass) {
        
        var arrayOfCities = Manager.shared.loadArrayOfCities()
        
        if arrayOfCities?.isEmpty == true {
            arrayOfCities?.insert(object, at: 0)
        } else {
            arrayOfCities?.removeFirst()
            arrayOfCities?.insert(object, at: 0)
        }
        UserDefaults.standard.set(encodable: arrayOfCities, forKey: "arrayWeatherClass")
    }
    
    func loadArrayOfCities() -> [WeatherClass]? {
        
        let weatherArray = UserDefaults.standard.value([WeatherClass].self, forKey: "arrayWeatherClass")
        
        if let weatherArray = weatherArray {
            return weatherArray
        } else {
            return []
        }
    }
    
    func addIcon(text: String) -> UIImage? {
        
        let image = { () -> UIImage in switch text {
        case "Sunny" :
            return UIImage(systemName: "sun.max")!
        case "Clear" :
            return UIImage(systemName: "moon")!
        case "Cloudy", "Overcast" :
            return UIImage(systemName: "cloud")!
        case "Partly cloudy" :
            return UIImage(systemName: "cloud.sun")!
        case "Fog", "Mist", "Freezing fog" :
            return UIImage(systemName: "cloud.fog")!
        case "Patchy rain possible", "Patchy light drizzle", "Light drizzle", "Light freezing rain", "Ice pellets", "Light rain shower", "Light showers of ice pellets" :
            return UIImage(systemName: "cloud.drizzle")!
        case "Patchy sleet possible", "Light sleet", "Light sleet showers", "Moderate or heavy sleet showers" :
            return UIImage(systemName: "cloud.sleet")!
        case "Patchy snow possible", "Moderate or heavy sleet", "Moderate or heavy snow showers" :
            return UIImage(systemName: "cloud.snow")!
        case "Patchy freezing drizzle possible", "Freezing drizzle", "Heavy freezing drizzle" :
            return UIImage(systemName: "thermometer.snowflake")!
        case "Patchy light rain", "Light rain", "Moderate rain at times", "Moderate rain" :
            return UIImage(systemName: "cloud.drizzle")!
        case "Thundery outbreaks possible", "Patchy light rain with thunder", "Moderate or heavy rain with thunder", "Patchy light snow with thunder", "Moderate or heavy snow with thunder" :
            return UIImage(systemName: "cloud.bolt.rain")!
        case "Heavy rain at times", "Heavy rain", "Moderate or heavy freezing rain", "Moderate or heavy rain shower", "Torrential rain shower", "Moderate or heavy showers of ice pellets" :
            return UIImage(named: "cloud.heavyrain")!
        case "Blowing snow", "Blizzard", "Patchy heavy snow", "Heavy snow" :
            return UIImage(systemName: "wind.snow")!
        case "Patchy light snow", "Light snow", "Patchy moderate snow", "Moderate snow", "Light snow showers" :
            return UIImage(systemName: "snow")!
        default:
            return UIImage(systemName: "cloud")!
        }
        }
        return image()
    }
    
    func addBackgroundImage (text: String, is_day: Int) -> UIImage? {
        
        // is_day 1 - YES, 0 - NO
        let image = { () -> UIImage in switch (text, is_day) {
        case ("Sunny", 1) :
            return UIImage(named: "sunny")!
        case ("Clear", 0) :
            return UIImage(named: "clear")!
        case ("Cloudy", 1), ("Partly cloudy", 1) :
            return UIImage(named: "cloudyDay")!
        case ("Cloudy", 0), ("Partly cloudy", 0) :
            return UIImage(named: "cloudyNight")!
        case ("Overcast", 1) :
            return UIImage(named: "overcastDay")!
        case ("Overcast", 0) :
            return UIImage(named: "overcastNight")!
        case ("Mist", 1) :
            return UIImage(named: "mistDay")!
        case ("Mist", 0) :
            return UIImage(named: "mistNight")!
        case ("Fog", 1) :
            return UIImage(named: "fogDay")!
        case ("Fog", 0) :
            return UIImage(named: "fogNight")!
        case ("Patchy rain possible", 1), ("Patchy light rain", 1), ("Light rain", 1), ("Moderate rain at times", 1), ("Moderate rain", 1), ("Heavy rain at times", 1), ("Heavy rain", 1), ("Light freezing rain", 1), ("Moderate or heavy freezing rain", 1), ("Light rain shower", 1), ("Moderate or heavy rain shower", 1), ("Torrential rain shower", 1), ("Patchy light rain with thunder", 1), ("Moderate or heavy rain with thunder", 1) :
            return UIImage(named: "rainDay")!
        case ("Patchy rain possible", 0), ("Patchy light rain", 0), ("Light rain", 0), ("Moderate rain at times", 0), ("Moderate rain", 0), ("Heavy rain at times", 0), ("Heavy rain", 0), ("Light freezing rain", 0), ("Moderate or heavy freezing rain", 0), ("Light rain shower", 0), ("Moderate or heavy rain shower", 0), ("Torrential rain shower", 0), ("Patchy light rain with thunder", 0), ("Moderate or heavy rain with thunder", 0) :
            return UIImage(named: "rainNight")!
        default:
            return UIImage(named: "sunny")!
        }
        }
        return image()
    }
    
    func addBackgroundImageToForecast (text: String) -> UIImage? {
        
        let image = { () -> UIImage in switch text {
        case "Sunny" :
            return UIImage(named: "sunny")!
        case "Cloudy", "Partly cloudy":
            return UIImage(named: "cloudyDay")!
        case "Overcast" :
            return UIImage(named: "overcastDay")!
        case "Mist" :
            return UIImage(named: "mistDay")!
        case "Fog":
            return UIImage(named: "fogDay")!
        case "Patchy rain possible", "Patchy light rain", "Light rain", "Moderate rain at times", "Moderate rain", "Heavy rain at times", "Heavy rain", "Light freezing rain", "Moderate or heavy freezing rain", "Light rain shower", "Moderate or heavy rain shower", "Torrential rain shower", "Patchy light rain with thunder", "Moderate or heavy rain with thunder":
            return UIImage(named: "rainDay")!
        default:
            return UIImage(named: "sunny")!
        }
        }
        return image()
    }
    
}
