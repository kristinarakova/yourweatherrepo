import UIKit

class ThreeDaysForecastViewController: UIViewController {
    
    @IBOutlet weak var firstDayForecastButton: UIButton!
    @IBOutlet weak var secondDayForecastButton: UIButton!
    @IBOutlet weak var thirdDayForecastButton: UIButton!
    @IBOutlet weak var searchButton: UIButton!
    
    @IBOutlet weak var textField: UITextField!
    
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var containerInfoView: UIView!
    @IBOutlet weak var firstDayForecastView: UIView!
    @IBOutlet weak var secondDayForecastView: UIView!
    @IBOutlet weak var thirdDayForecastView: UIView!
    
    @IBOutlet weak var infoForecastView: UIView!
    @IBOutlet weak var sunriseView: UIView!
    @IBOutlet weak var sunsetView: UIView!
    @IBOutlet weak var chanceRainView: UIView!
    @IBOutlet weak var humidityView: UIView!
    @IBOutlet weak var windView: UIView!
    @IBOutlet weak var preciptationView: UIView!
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var toolBar: UIToolbar!
    @IBOutlet weak var refreshBarButtonItem: UIBarButtonItem!
    @IBOutlet weak var currentWeatherBarButtonItem: UIBarButtonItem!
    @IBOutlet weak var listCitiesBarButtonItem: UIBarButtonItem!
    
    @IBOutlet weak var threeDayForecastLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var countryLabel: UILabel!
    @IBOutlet weak var lastUpdatedLabel: UILabel!
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    
    @IBOutlet weak var sunriseLabel: UILabel!
    @IBOutlet weak var sunsetLabel: UILabel!
    @IBOutlet weak var chanceRainLabel: UILabel!
    @IBOutlet weak var humidityLabel: UILabel!
    @IBOutlet weak var windLabel: UILabel!
    @IBOutlet weak var preciptationLabel: UILabel!
    
    @IBOutlet weak var sunriseValueLabel: UILabel!
    @IBOutlet weak var sunsetValueLabel: UILabel!
    @IBOutlet weak var chanceRainValueLabel: UILabel!
    @IBOutlet weak var humidityValueLabel: UILabel!
    @IBOutlet weak var windValueLabel: UILabel!
    @IBOutlet weak var preciptationValueLabel: UILabel!
    
    @IBOutlet weak var firstDayLabel: UILabel!
    @IBOutlet weak var secondDayLabel: UILabel!
    @IBOutlet weak var thirdDayLabel: UILabel!
    
    @IBOutlet weak var firstDayMaxTempLabel: UILabel!
    @IBOutlet weak var firstDayMinTempLabel: UILabel!
    @IBOutlet weak var secondDayMaxTempLabel: UILabel!
    @IBOutlet weak var secondDayMinTempLabel: UILabel!
    @IBOutlet weak var thirdDayMaxTempLabel: UILabel!
    @IBOutlet weak var thirdDayMinTempLabel: UILabel!
    
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var infoIconImageView: UIImageView!
    @IBOutlet weak var firstDayIconImageView: UIImageView!
    @IBOutlet weak var secondDayIconImageView: UIImageView!
    @IBOutlet weak var thirdDayIconImageView: UIImageView!
    
    private var city: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addInformation()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.textColor(color: .white)
        
        self.hideKeyboard()
        self.textField.delegate = self
    }
    
    @IBAction func searchButton(_ sender: UIButton) {
        
        guard let city = self.textField.text else {return}
        self.city = city
        
        self.sendCityRequest()
    }
    
    @IBAction func currentWeatherBarButtonItem(_ sender: UIBarButtonItem) {
        
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func listCitiesBarButtonItem(_ sender: UIBarButtonItem) {
        
        guard let listCitiesViewController = storyboard?.instantiateViewController(withIdentifier: "ListCitiesViewController") as? ListCitiesViewController else {return}
        self.navigationController?.pushViewController(listCitiesViewController, animated: true)
    }
    
    @IBAction func refreshBarButtonItem(_ sender: UIBarButtonItem) {
        
        if let city = Manager.shared.jsonWeather?.location?.name {
            Manager.shared.refreshData(city: city) {
                self.addInformation()
            }
        }
    }
    
    private func sendCityRequest () {
        
        Manager.shared.sendCurrentWeatherRequestByCity(city: city) {
            self.addInformation()
        }
    }
    
    private func addInformation () {
        
        if let name = Manager.shared.jsonWeather?.location?.name,
            let country = Manager.shared.jsonWeather?.location?.country,
            let last_updated = Manager.shared.jsonWeather?.current?.last_updated,
            
            let date = Manager.shared.jsonWeather?.forecast?.forecastday?.first?.date,
            let text = Manager.shared.jsonWeather?.forecast?.forecastday?.first?.day?.condition?.text,
            let avgtemp_c = Manager.shared.jsonWeather?.forecast?.forecastday?.first?.day?.avgtemp_c,
            
            let sunrise = Manager.shared.jsonWeather?.forecast?.forecastday?.first?.astro?.sunrise,
            let sunset = Manager.shared.jsonWeather?.forecast?.forecastday?.first?.astro?.sunset,
            
            let daily_chance_of_rain = Manager.shared.jsonWeather?.forecast?.forecastday?.first?.day?.daily_chance_of_rain,
            let avghumidity = Manager.shared.jsonWeather?.forecast?.forecastday?.first?.day?.avghumidity,
            let maxwind_kph = Manager.shared.jsonWeather?.forecast?.forecastday?.first?.day?.maxwind_kph,
            let totalprecip_mm = Manager.shared.jsonWeather?.forecast?.forecastday?.first?.day?.totalprecip_mm,
            
            let firstDay = Manager.shared.jsonWeather?.forecast?.forecastday?[0].date,
            let secondDay = Manager.shared.jsonWeather?.forecast?.forecastday?[1].date,
            let thirdDay = Manager.shared.jsonWeather?.forecast?.forecastday?[2].date,
            let firstMaxTemp = Manager.shared.jsonWeather?.forecast?.forecastday?[0].day?.maxtemp_c,
            let firstMinTemp = Manager.shared.jsonWeather?.forecast?.forecastday?[0].day?.mintemp_c,
            let secondMaxTemp = Manager.shared.jsonWeather?.forecast?.forecastday?[1].day?.maxtemp_c,
            let secondMinTemp = Manager.shared.jsonWeather?.forecast?.forecastday?[1].day?.mintemp_c,
            let thirdMaxTemp = Manager.shared.jsonWeather?.forecast?.forecastday?[2].day?.maxtemp_c,
            let thirdMinTemp = Manager.shared.jsonWeather?.forecast?.forecastday?[2].day?.mintemp_c,
            
            let textFirstDayForecast = Manager.shared.jsonWeather?.forecast?.forecastday?[0].day?.condition?.text,
            let textSecondDayForecast = Manager.shared.jsonWeather?.forecast?.forecastday?[1].day?.condition?.text,
            let textThirdDayForecast = Manager.shared.jsonWeather?.forecast?.forecastday?[2].day?.condition?.text {
            
            let lastUpdated = self.dateFormatter(data: last_updated, oldFormat: "yyyy-MM-dd HH:mm", newFormat: "MMM d, h:mm a")
            let day = self.dateFormatter(data: date, oldFormat: "yyyy-MM-dd", newFormat: "MMM d")
            
            let firstDayFormatter = self.dateFormatter(data: firstDay, oldFormat: "yyyy-MM-dd", newFormat: "MMM d")
            let secondDayFormatter = self.dateFormatter(data: secondDay, oldFormat: "yyyy-MM-dd", newFormat: "MMM d")
            let thirdDayFormatter = self.dateFormatter(data: thirdDay, oldFormat: "yyyy-MM-dd", newFormat: "MMM d")
            
            self.cityLabel.text = name
            self.countryLabel.text = country
            self.lastUpdatedLabel.text = lastUpdated
            
            self.dateLabel.text = day
            self.textLabel.text = text
            self.temperatureLabel.text = "\(avgtemp_c)°"
            
            self.sunriseValueLabel.text = sunrise
            self.sunsetValueLabel.text = sunset
            
            self.chanceRainValueLabel.text = "\(daily_chance_of_rain)%"
            self.humidityValueLabel.text = "\(avghumidity)%"
            self.windValueLabel.text = "\(maxwind_kph) km/h"
            self.preciptationValueLabel.text = "\(totalprecip_mm) mm"
            
            self.firstDayLabel.text = firstDayFormatter
            self.secondDayLabel.text = secondDayFormatter
            self.thirdDayLabel.text = thirdDayFormatter
            self.firstDayMaxTempLabel.text = "\(firstMaxTemp)°"
            self.firstDayMinTempLabel.text = "\(firstMinTemp)°"
            self.secondDayMaxTempLabel.text = "\(secondMaxTemp)°"
            self.secondDayMinTempLabel.text = "\(secondMinTemp)°"
            self.thirdDayMaxTempLabel.text = "\(thirdMaxTemp)°"
            self.thirdDayMinTempLabel.text = "\(thirdMinTemp)°"
            
            self.backgroundImageView.image = Manager.shared.addBackgroundImageToForecast(text: text)
            self.infoIconImageView.image = Manager.shared.addIcon(text: text)
            self.firstDayIconImageView.image = Manager.shared.addIcon(text: textFirstDayForecast)
            self.secondDayIconImageView.image = Manager.shared.addIcon(text: textSecondDayForecast)
            self.thirdDayIconImageView.image = Manager.shared.addIcon(text: textThirdDayForecast)
        }
    }
    
    @IBAction func firstDayForecastButton(_ sender: UIButton) {
        
        if  let date = Manager.shared.jsonWeather?.forecast?.forecastday?[0].date,
            let text = Manager.shared.jsonWeather?.forecast?.forecastday?[0].day?.condition?.text,
            let avgtemp_c = Manager.shared.jsonWeather?.forecast?.forecastday?[0].day?.avgtemp_c,
            
            let sunrise = Manager.shared.jsonWeather?.forecast?.forecastday?[0].astro?.sunrise,
            let sunset = Manager.shared.jsonWeather?.forecast?.forecastday?[0].astro?.sunset,
            
            let daily_chance_of_rain = Manager.shared.jsonWeather?.forecast?.forecastday?[0].day?.daily_chance_of_rain,
            let avghumidity = Manager.shared.jsonWeather?.forecast?.forecastday?[0].day?.avghumidity,
            let maxwind_kph = Manager.shared.jsonWeather?.forecast?.forecastday?[0].day?.maxwind_kph,
            let totalprecip_mm = Manager.shared.jsonWeather?.forecast?.forecastday?[0].day?.totalprecip_mm {
            
            let day = self.dateFormatter(data: date, oldFormat: "yyyy-MM-dd", newFormat: "MMM d")
            
            self.dateLabel.text = day
            self.textLabel.text = text
            self.temperatureLabel.text = "\(avgtemp_c)°"
            
            self.sunriseValueLabel.text = sunrise
            self.sunsetValueLabel.text = sunset
            
            self.chanceRainValueLabel.text = "\(daily_chance_of_rain)%"
            self.humidityValueLabel.text = "\(avghumidity)%"
            self.windValueLabel.text = "\(maxwind_kph) km/h"
            self.preciptationValueLabel.text = "\(totalprecip_mm) mm"
            
            self.backgroundImageView.image = Manager.shared.addBackgroundImageToForecast(text: text)
            self.infoIconImageView.image = Manager.shared.addIcon(text: text)
        }
    }
    
    @IBAction func secondDayForecastButton(_ sender: UIButton) {
        
        if  let date = Manager.shared.jsonWeather?.forecast?.forecastday?[1].date,
            let text = Manager.shared.jsonWeather?.forecast?.forecastday?[1].day?.condition?.text,
            let avgtemp_c = Manager.shared.jsonWeather?.forecast?.forecastday?[1].day?.avgtemp_c,
            
            let sunrise = Manager.shared.jsonWeather?.forecast?.forecastday?[1].astro?.sunrise,
            let sunset = Manager.shared.jsonWeather?.forecast?.forecastday?[1].astro?.sunset,
            
            let daily_chance_of_rain = Manager.shared.jsonWeather?.forecast?.forecastday?[1].day?.daily_chance_of_rain,
            let avghumidity = Manager.shared.jsonWeather?.forecast?.forecastday?[1].day?.avghumidity,
            let maxwind_kph = Manager.shared.jsonWeather?.forecast?.forecastday?[1].day?.maxwind_kph,
            let totalprecip_mm = Manager.shared.jsonWeather?.forecast?.forecastday?[1].day?.totalprecip_mm {
            
            let day = self.dateFormatter(data: date, oldFormat: "yyyy-MM-dd", newFormat: "MMM d")
            
            self.dateLabel.text = day
            self.textLabel.text = text
            self.temperatureLabel.text = "\(avgtemp_c)°"
            
            self.sunriseValueLabel.text = sunrise
            self.sunsetValueLabel.text = sunset
            
            self.chanceRainValueLabel.text = "\(daily_chance_of_rain)%"
            self.humidityValueLabel.text = "\(avghumidity)%"
            self.windValueLabel.text = "\(maxwind_kph) km/h"
            self.preciptationValueLabel.text = "\(totalprecip_mm) mm"
            
            self.backgroundImageView.image = Manager.shared.addBackgroundImageToForecast(text: text)
            self.infoIconImageView.image = Manager.shared.addIcon(text: text)
        }
    }
    
    @IBAction func thirdDayForecastButton(_ sender: UIButton) {
        
        if  let date = Manager.shared.jsonWeather?.forecast?.forecastday?[2].date,
            let text = Manager.shared.jsonWeather?.forecast?.forecastday?[2].day?.condition?.text,
            let avgtemp_c = Manager.shared.jsonWeather?.forecast?.forecastday?[2].day?.avgtemp_c,
            
            let sunrise = Manager.shared.jsonWeather?.forecast?.forecastday?[2].astro?.sunrise,
            let sunset = Manager.shared.jsonWeather?.forecast?.forecastday?[2].astro?.sunset,
            
            let daily_chance_of_rain = Manager.shared.jsonWeather?.forecast?.forecastday?[2].day?.daily_chance_of_rain,
            let avghumidity = Manager.shared.jsonWeather?.forecast?.forecastday?[2].day?.avghumidity,
            let maxwind_kph = Manager.shared.jsonWeather?.forecast?.forecastday?[2].day?.maxwind_kph,
            let totalprecip_mm = Manager.shared.jsonWeather?.forecast?.forecastday?[2].day?.totalprecip_mm {
            
            let day = self.dateFormatter(data: date, oldFormat: "yyyy-MM-dd", newFormat: "MMM d")
            
            self.dateLabel.text = day
            self.textLabel.text = text
            self.temperatureLabel.text = "\(avgtemp_c)°"
            
            self.sunriseValueLabel.text = sunrise
            self.sunsetValueLabel.text = sunset
            
            self.chanceRainValueLabel.text = "\(daily_chance_of_rain)%"
            self.humidityValueLabel.text = "\(avghumidity)%"
            self.windValueLabel.text = "\(maxwind_kph) km/h"
            self.preciptationValueLabel.text = "\(totalprecip_mm) mm"
            
            self.backgroundImageView.image = Manager.shared.addBackgroundImageToForecast(text: text)
            self.infoIconImageView.image = Manager.shared.addIcon(text: text)
        }
    }
    
    private func textColor(color: UIColor) {
        self.threeDayForecastLabel.textColor = color
        self.cityLabel.textColor = color
        self.countryLabel.textColor = color
        self.lastUpdatedLabel.textColor = color
        
        self.dateLabel.textColor = color
        self.textLabel.textColor = color
        self.temperatureLabel.textColor = color
        
        self.sunriseLabel.textColor = color
        self.sunsetLabel.textColor = color
        self.chanceRainLabel.textColor = color
        self.humidityLabel.textColor = color
        self.windLabel.textColor = color
        self.preciptationLabel.textColor = color
        
        self.sunriseValueLabel.textColor = color
        self.sunsetValueLabel.textColor = color
        self.chanceRainValueLabel.textColor = color
        self.humidityValueLabel.textColor = color
        self.windValueLabel.textColor = color
        self.preciptationValueLabel.textColor = color
        
        self.firstDayLabel.textColor = color
        self.secondDayLabel.textColor = color
        self.thirdDayLabel.textColor = color
        
        self.firstDayMaxTempLabel.textColor = color
        self.firstDayMinTempLabel.textColor = color
        self.secondDayMaxTempLabel.textColor = color
        self.secondDayMinTempLabel.textColor = color
        self.thirdDayMaxTempLabel.textColor = color
        self.thirdDayMinTempLabel.textColor = color
    }
    
}
