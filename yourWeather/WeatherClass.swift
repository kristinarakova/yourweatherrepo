import Foundation

class WeatherClass: Codable {
    var location: LocationClass? = nil
    var current: CurrentClass? = nil
    var forecast: ForecastClass? = nil
}

class LocationClass: Codable {
    
    var name: String? = nil
    var region: String? = nil
    var country: String? = nil
    var lat: Double? = nil
    var lon: Double? = nil
    var tz_id: String? = nil
    var localtime_epoch: Int? = nil
    var localtime: String? = nil
}

class CurrentClass: Codable {
    
    var last_updated: String? = nil
    var temp_c: Double? = nil
    var is_day: Int? = nil
    var condition: CurrentConditionClass? = nil
    var wind_kph: Double? = nil
    var pressure_mb: Double? = nil
    var humidity: Int? = nil
    var cloud: Int? = nil
    var feelslike_c: Double? = nil
    var uv: Int? = nil
}

class CurrentConditionClass: Codable {
    
    var text: String? = nil
    var icon: String? = nil
}

class ForecastClass: Codable {
    
    var forecastday: [ForecastdayClass]? = nil
}

class ForecastdayClass: Codable {
    
    var date: String? = nil
    var day: ForecastdayDayClass? = nil
    var astro: ForecastdayAstroClass? = nil
}

class ForecastdayDayClass: Codable {

    var maxtemp_c: Double? = nil
    var mintemp_c: Double? = nil
    var avgtemp_c: Double? = nil
    var maxwind_kph: Double? = nil
    var totalprecip_mm: Double? = nil
    var avgvis_km: Double? = nil
    var avghumidity: Double? = nil
    var daily_will_it_rain: Int? = nil
    var daily_chance_of_rain: String? = nil
    var condition: ForecastdayDayConditionClass? = nil
    var uv: Int? = nil
}

class ForecastdayDayConditionClass: Codable {

    var text: String? = nil
    var icon: String? = nil
}

class ForecastdayAstroClass: Codable {

    var sunrise: String? = nil
    var sunset: String? = nil
}
