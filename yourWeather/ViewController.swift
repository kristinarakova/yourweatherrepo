import UIKit
import CoreLocation

class ViewController: UIViewController, CLLocationManagerDelegate {
    
    @IBOutlet weak var toolBar: UIToolbar!
    @IBOutlet weak var refreshBarButtonItem: UIBarButtonItem!
    @IBOutlet weak var forecastBarButtonItem: UIBarButtonItem!
    @IBOutlet weak var listCitiesBarButtonItem: UIBarButtonItem!
    
    @IBOutlet weak var backgroundImageView: UIImageView!
    
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var menuBarView: UIView!
    
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var searchButton: UIButton!
    
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var countryLabel: UILabel!
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var lastUpdatedLabel: UILabel!
    
    @IBOutlet weak var sunriseView: UIView!
    @IBOutlet weak var sunsetView: UIView!
    @IBOutlet weak var chanceRainView: UIView!
    @IBOutlet weak var humidityView: UIView!
    @IBOutlet weak var windView: UIView!
    @IBOutlet weak var feelsLikeView: UIView!
    @IBOutlet weak var percipitationView: UIView!
    @IBOutlet weak var pressureView: UIView!
    @IBOutlet weak var visibilityView: UIView!
    @IBOutlet weak var uvIndexView: UIView!
    
    @IBOutlet weak var sunriseLabel: UILabel!
    @IBOutlet weak var sunsetLabel: UILabel!
    @IBOutlet weak var chanceRainLabel: UILabel!
    @IBOutlet weak var humidityLabel: UILabel!
    @IBOutlet weak var windLabel: UILabel!
    @IBOutlet weak var feelsLikeLabel: UILabel!
    @IBOutlet weak var precipitationLabel: UILabel!
    @IBOutlet weak var pressureLabel: UILabel!
    @IBOutlet weak var visibilityLabel: UILabel!
    @IBOutlet weak var uvIndexLabel: UILabel!
    
    @IBOutlet weak var sunriseValueLabel: UILabel!
    @IBOutlet weak var sunsetValueLabel: UILabel!
    @IBOutlet weak var chanceRainValueLabel: UILabel!
    @IBOutlet weak var humidityValueLabel: UILabel!
    @IBOutlet weak var windValueLabel: UILabel!
    @IBOutlet weak var feelsLikeValueLabel: UILabel!
    @IBOutlet weak var precipitationValueLabel: UILabel!
    @IBOutlet weak var pressureValueLabel: UILabel!
    @IBOutlet weak var visibilityValueLabel: UILabel!
    @IBOutlet weak var uvIndexValueLabel: UILabel!
    
    private let locationManager = CLLocationManager()
    
    private var coordinate: String = ""
    private var city: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addInformation()
        
        self.gettingLocation()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.addInformation()
        
        self.textColor(color: .white)
        
        self.hideKeyboard()
        self.searchTextField.delegate = self
    }
    
    @IBAction func forecastBarButtonItem(_ sender: UIBarButtonItem) {
        
        guard let threeDaysForecastViewController = storyboard?.instantiateViewController(withIdentifier: "ThreeDaysForecastViewController") as? ThreeDaysForecastViewController else {return}
        self.navigationController?.pushViewController(threeDaysForecastViewController, animated: true)
    }
    
    @IBAction func listCitiesBarButtonItem(_ sender: UIBarButtonItem) {
        
        guard let listCitiesViewController = storyboard?.instantiateViewController(withIdentifier: "ListCitiesViewController") as? ListCitiesViewController else {return}
        self.navigationController?.pushViewController(listCitiesViewController, animated: true)
    }
    
    @IBAction func refreshBarButtonItem(_ sender: UIBarButtonItem) {
        
        if let city = Manager.shared.jsonWeather?.location?.name {
            Manager.shared.refreshData(city: city) {
                self.addInformation()
            }
        }
    }
    
    @IBAction func searchButton(_ sender: UIButton) {
        
        guard let city = self.searchTextField.text else {return}
        self.city = city
        
        self.sendCityRequest()
    }
    
    func gettingLocation () {
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
    }
    
    func locationManager (_ manager: CLLocationManager,
                          didUpdateLocations locations: [CLLocation]) {
        
        let location: CLLocationCoordinate2D = manager.location!.coordinate
        self.locationManager.stopUpdatingLocation()
        
        self.coordinate = "\(location.latitude),\(location.longitude)"
        
        Manager.shared.sendCurrentWeatherRequestByCoordinate(coordinate: coordinate) {
            self.addInformation()
        }
    }
    
    private func sendCityRequest () {
        
        Manager.shared.sendCurrentWeatherRequestByCity(city: city) {
            self.addInformation()
        }
    }
    
    private func addInformation () {
        
        DispatchQueue.main.async {
            if let name = Manager.shared.jsonWeather?.location?.name,
               let country = Manager.shared.jsonWeather?.location?.country,
               let text = Manager.shared.jsonWeather?.current?.condition?.text,
               let temp_c = Manager.shared.jsonWeather?.current?.temp_c,
               let last_updated = Manager.shared.jsonWeather?.current?.last_updated,
               let sunrise = Manager.shared.jsonWeather?.forecast?.forecastday?.first?.astro?.sunrise,
               let sunset = Manager.shared.jsonWeather?.forecast?.forecastday?.first?.astro?.sunset,
               let daily_chance_of_rain = Manager.shared.jsonWeather?.forecast?.forecastday?.first?.day?.daily_chance_of_rain,
               let avghumidity = Manager.shared.jsonWeather?.forecast?.forecastday?.first?.day?.avghumidity,
               let wind_kph = Manager.shared.jsonWeather?.current?.wind_kph,
               let feelslike_c = Manager.shared.jsonWeather?.current?.feelslike_c,
               let totalprecip_mm = Manager.shared.jsonWeather?.forecast?.forecastday?.first?.day?.totalprecip_mm,
               let pressure_mb = Manager.shared.jsonWeather?.current?.pressure_mb,
               let avgvis_km = Manager.shared.jsonWeather?.forecast?.forecastday?.first?.day?.avgvis_km,
               let uv = Manager.shared.jsonWeather?.forecast?.forecastday?.first?.day?.uv,
               let is_day = Manager.shared.jsonWeather?.current?.is_day {
                
                let date = self.dateFormatter(data: last_updated, oldFormat: "yyyy-MM-dd HH:mm", newFormat: "MMM d, h:mm a")
                
                self.cityLabel.text = name
                self.countryLabel.text = country
                self.textLabel.text = text
                self.temperatureLabel.text = "\(temp_c)°"
                self.lastUpdatedLabel.text = date
                self.sunriseValueLabel.text = sunrise
                self.sunsetValueLabel.text = sunset
                self.chanceRainValueLabel.text = "\(daily_chance_of_rain)%"
                self.humidityValueLabel.text = "\(avghumidity)%"
                self.windValueLabel.text = "\(wind_kph) km/h"
                self.feelsLikeValueLabel.text = "\(feelslike_c)°"
                self.precipitationValueLabel.text = "\(totalprecip_mm) mm"
                self.pressureValueLabel.text = "\(pressure_mb) hPa"
                self.visibilityValueLabel.text = "\(avgvis_km) km"
                self.uvIndexValueLabel.text = String(uv)
                
                self.backgroundImageView.image = Manager.shared.addBackgroundImage(text: text, is_day: is_day)
            }
        }
    }
    
    private func textColor(color: UIColor) {
        self.cityLabel.textColor = color
        self.countryLabel.textColor = color
        self.textLabel.textColor = color
        self.temperatureLabel.textColor = color
        self.lastUpdatedLabel.textColor = color
        
        self.sunriseLabel.textColor = color
        self.sunsetLabel.textColor = color
        self.chanceRainLabel.textColor = color
        self.humidityLabel.textColor = color
        self.windLabel.textColor = color
        self.feelsLikeLabel.textColor = color
        self.precipitationLabel.textColor = color
        self.pressureLabel.textColor = color
        self.visibilityLabel.textColor = color
        self.uvIndexLabel.textColor = color
        
        self.sunriseValueLabel.textColor = color
        self.sunsetValueLabel.textColor = color
        self.chanceRainValueLabel.textColor = color
        self.humidityValueLabel.textColor = color
        self.windValueLabel.textColor = color
        self.feelsLikeValueLabel.textColor = color
        self.precipitationValueLabel.textColor = color
        self.pressureValueLabel.textColor = color
        self.visibilityValueLabel.textColor = color
        self.uvIndexValueLabel.textColor = color
    }
    
}
